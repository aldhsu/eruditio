# How to run example application
1. Clone the repo
1. Run `bundle`
1. Run `rake create_example:bootstrap`
1. Start rails server

# Features
1. Set a student's progress, you will have to find a valid combination of student and lesson id
  - `curl -H "Content-Type: application/vnd.api+json" -X POST -d '{"data":{"relationships":{"student":{"data":{"type":"students","id":"student_id"}},"lesson":{"data":{"type":"lessons","id":"lesson_id"}}},"type":"student-lessons"}}' http://localhost:3000/student-lessons`
1. Get a student's progress, will return a list of the students completed lesson ids
  - `curl 'http://localhost:3200/students/{student_id}/lessons?fields%5Blessons%5D=id'`
1. Generate a report for a teachers classes, you will have to find the teacher id from rails db
  - `curl 'localhost:3000/reports/cohort?teacher_id={teacher_id}'`

# Design Brief
## Coding Project - Ruby

Company X is developing a new app for student education. Students complete lessons and their progress is recorded.
Each lesson has 3 parts - 1, 2 and 3. There are 100 lessons in total.

###PART 1

Generate a rails app that persists students and their progress.

Define routes for:
1. setting a student's progress - progress should consist of a lesson and part number.
1. returning a JSON representation of a student and their associated progress.


###PART 2

Teachers have classes containing number of students.

1. Add a teacher model that is related to students
1. Create a reports page for a teacher to view progress all of their students.

###PART 3

Calculating progress

1. add a method for updating student progress - this should verify that the
student is only able to complete the next part number in sequence e.g.

L1 P1, L1 P2, L1 P3, L2 P1, L2 P2 etc

# Work notes
### Part 1
1. I am interpreting `persists students and their progress` to mean build an API that persists can create students and an end point that will take their progress. It is implied that models will have to be created.

#### My strategy is:
  1. For students: Use single table inheritance(STI) as students and teachers will be pretty similar and eventually admin as well
  1. For lessons and parts: Use a singular table with parent and child relationships so lessons and parts can be treated similarly. This has added benefits for flexibility, lessons can be arbitrarily nested and have any number of `parts`.
  1. Persisting progress will be done through a separate table
    - Since I am using STI and want to keep the table clean from subtype columns I think it is justified and it allows the user in the future to be completing multiple `lesson` tracks eg. Maths, English, Science
    - It would also allow tracking the answers or results of the lesson but that is out of scope
    - Using a singular table for both lessons means we no longer need to send part and lesson and only need to send lesson id
    - I will be using JSONAPI-Resources to render well formatted payloads that is the default for Ember front ends rather than defining our own structure and sideloading with ActiveModelSerializers
      - This also allows dynamically requesting progress information from the front without hardcoding sideloads in serializers
  1. It is not defined who owns the lessons ie. teachers or the organisation so that will be considered out of scope
  1. NB: I am ignoring authentication as it is not asked for, the system is prototype completing the tasks required

### Part 2
  1. I will continue to use STI for teachers
  1. Teachers will be related to students through a `cohort`, its fits my Latin theme and I don't want to deal with overwriting the `class` method
  1. Using STI is now paying dividends because we can easily extend the base user class so that students and teachers can have many cohorts through a join table
  1. Since progress is not specified on the part or lesson level, I will return the sum total of the all lessons and parts but can be sliced differently if necessary

### Part 3
  1. Since we are creating a new `StudentLesson` every time we can just use a validator on the model
    - I will use preorder traversal of the tree for producing the sequence
    - Slightly different as it will be L1, P1, P2, P3, L2, ...
    - This is a design decision as it would also be easy to navigate through leaf nodes alone
    - It allows a preamble to the lesson that may contain information needed to complete the parts but no tasks in itself
    - I have left in the leaf traversal to complete the task to satisfy the task and you can see in the code where it would plumb in.