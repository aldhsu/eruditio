Rails.application.routes.draw do
  jsonapi_resources(:students) { jsonapi_relationships }
  jsonapi_resources(:student_lessons) { jsonapi_relationships }
  jsonapi_resources(:lessons) { jsonapi_relationships }

  scope :reports do
    get "cohort", to: "reports#cohort"
  end
end
