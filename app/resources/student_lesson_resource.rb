class StudentLessonResource < JSONAPI::Resource
  has_one :student
  has_one :lesson
end
