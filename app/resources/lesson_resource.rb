class LessonResource < JSONAPI::Resource
  attributes :position, :content, :title
  has_one :parent
  has_many :parts
  has_many :students
  has_many :student_lessons
end
