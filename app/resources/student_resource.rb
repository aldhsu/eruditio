class StudentResource < JSONAPI::Resource
  attributes :first_name, :last_name, :email, :type
  has_many :lessons
  has_many :student_lessons
end
