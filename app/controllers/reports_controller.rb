class ReportsController < ApplicationController
  def cohort
    teacher = Teacher.find_by(id: params[:teacher_id])
    if teacher
      report = teacher.class_reports
      render json: report
    else
      head :not_found
    end
  end
end
