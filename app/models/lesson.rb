# == Schema Information
#
# Table name: lessons
#
#  id         :integer          not null, primary key
#  parent_id  :integer
#  position   :integer          default(1), not null
#  content    :text
#  title      :text
#  created_at :datetime
#  updated_at :datetime
#

class Lesson < ActiveRecord::Base
  belongs_to :parent, class_name: "Lesson"
  has_many :parts, class_name: "Lesson", foreign_key: "parent_id", dependent: :destroy
  has_many :students, through: :student_lessons
  has_many :student_lessons

  validates :position, presence: true
  validates :position, uniqueness: { scope: :parent_id }

  def previous_lesson_id
    preordered_index(-1)
  end

  def next_lesson_id
    preordered_index(1)
  end

  def leaf_node_index(offset)
    leaf_nodes = self.class.flatten_in_preorder(leaf_only: true)
    current_index = leaf_nodes.find_index(id)

    return nil if current_index + offset < 0
    leaf_nodes[current_index + offset]
  end

  def preordered_index(offset)
    all_nodes = self.class.flatten_in_preorder
    current_index = all_nodes.find_index(id)

    return nil if current_index + offset < 0
    all_nodes[current_index + offset]
  end

  class << self
    def leaf_filter
      "WHERE children.id NOT IN (SELECT DISTINCT parent_id from all_lessons WHERE parent_id IS NOT NULL)"
    end

    def flatten_in_preorder(options = {})
      sql = "WITH RECURSIVE all_lessons AS (
      SELECT
        id,
        parent_id,
        0 as level,
        position,
        ARRAY[position] AS path
      FROM lessons
      WHERE parent_id IS NULL

      UNION

      SELECT
        children.id,
        children.parent_id,
        all_lessons.level + 1 as level,
        children.position,
        all_lessons.path || children.position
      FROM lessons children, all_lessons
      WHERE children.parent_id = all_lessons.id
      )

      SELECT children.id from all_lessons children
        #{leaf_filter if options[:leaf_only]}
      ORDER by children.path
      "

      ActiveRecord::Base.connection.exec_query(sql)
        .to_a
        .map { |record| record["id"].to_i }
    end
  end
end
