# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  type       :string           not null
#  first_name :string           not null
#  last_name  :string           not null
#  email      :string           not null
#  created_at :datetime
#  updated_at :datetime
#

class User < ActiveRecord::Base
  validates :type, :first_name, :last_name, :email, presence: true

  has_and_belongs_to_many :cohorts
end
