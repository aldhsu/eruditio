class Teacher < User
  def class_reports
    sql = "
    SELECT
      COUNT(student_lessons),
      users.first_name,
      users.last_name
    FROM cohorts
    INNER JOIN cohorts_users ON cohorts.id = cohorts_users.cohort_id
    INNER JOIN users ON users.id = cohorts_users.user_id
    LEFT JOIN student_lessons ON users.id = student_lessons.student_id
    WHERE cohorts_users.cohort_id in (?) AND users.type = 'Student'
    GROUP by users.id
    "

    # Could use a recursive query to roll up the count to a certain level
    # Alternatively, since parts are where the activities live and they are all `leaves`
    # we could count them using our sweet Lesson.flatten_in_preorder
    sanitized_query = self.class.send(:sanitize_sql_array, [sql, cohorts.pluck(:id)])
    ActiveRecord::Base.connection.exec_query(sanitized_query).to_a
  end
end
