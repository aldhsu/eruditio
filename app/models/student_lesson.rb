# == Schema Information
#
# Table name: student_lessons
#
#  id         :integer          not null, primary key
#  student_id :integer          not null
#  lesson_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class StudentLesson < ActiveRecord::Base
  validates :student, :lesson, presence: true
  validate :check_duplicate
  validate :lesson_is_next, if: Proc.new { |r| r.student && r.lesson }
  belongs_to :student
  belongs_to :lesson

  def lesson_is_next
    previous_lesson_id = lesson.previous_lesson_id
    return unless previous_lesson_id

    has_completed = student.student_lessons.where(lesson_id: previous_lesson_id).any?
    errors.add(:base, "You have not completed the previous lessons yet.") unless has_completed
  end

  def check_duplicate
    record = StudentLesson.find_by(student: student, lesson: lesson)
    errors.add(:base, "Duplicate record exists for this student and lesson.") if record
  end
end
