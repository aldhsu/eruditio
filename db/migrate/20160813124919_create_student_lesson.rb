class CreateStudentLesson < ActiveRecord::Migration
  def change
    create_table :student_lessons do |t|
      t.integer :student_id, index: true, null: false
      t.integer :lesson_id, index: true, null: false

      t.timestamps
    end
  end
end
