class CreateLessonHierarchies < ActiveRecord::Migration
  def change
    create_table :lesson_hierarchies, id: false do |t|
      t.integer :ancestor_id, null: false
      t.integer :descendant_id, null: false
      t.integer :generations, null: false
    end

    add_index :lesson_hierarchies, [:ancestor_id, :descendant_id, :generations],
      unique: true,
      name: "lesson_anc_desc_idx"

    add_index :lesson_hierarchies, [:descendant_id],
      name: "lesson_desc_idx"
  end
end
