class CreateLesson < ActiveRecord::Migration
  def change
    create_table :lessons do |t|
      t.integer :parent_id, index: true
      t.integer :position, default: 1, null: false
      t.text :content
      t.text :title

      t.timestamps
    end
  end
end
