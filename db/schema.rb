# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160814091822) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "cohorts", force: :cascade do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cohorts_users", id: false, force: :cascade do |t|
    t.integer "cohort_id", null: false
    t.integer "user_id",   null: false
  end

  add_index "cohorts_users", ["cohort_id", "user_id"], name: "index_cohorts_users_on_cohort_id_and_user_id", using: :btree
  add_index "cohorts_users", ["user_id", "cohort_id"], name: "index_cohorts_users_on_user_id_and_cohort_id", using: :btree

  create_table "lesson_hierarchies", id: false, force: :cascade do |t|
    t.integer "ancestor_id",   null: false
    t.integer "descendant_id", null: false
    t.integer "generations",   null: false
  end

  add_index "lesson_hierarchies", ["ancestor_id", "descendant_id", "generations"], name: "lesson_anc_desc_idx", unique: true, using: :btree
  add_index "lesson_hierarchies", ["descendant_id"], name: "lesson_desc_idx", using: :btree

  create_table "lessons", force: :cascade do |t|
    t.integer  "parent_id"
    t.integer  "position",   default: 1, null: false
    t.text     "content"
    t.text     "title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "lessons", ["parent_id"], name: "index_lessons_on_parent_id", using: :btree

  create_table "student_lessons", force: :cascade do |t|
    t.integer  "student_id", null: false
    t.integer  "lesson_id",  null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "student_lessons", ["lesson_id"], name: "index_student_lessons_on_lesson_id", using: :btree
  add_index "student_lessons", ["student_id"], name: "index_student_lessons_on_student_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "type",       null: false
    t.string   "first_name", null: false
    t.string   "last_name",  null: false
    t.string   "email",      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
