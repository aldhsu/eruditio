namespace :create_example do
  desc "creates an example setup"
  task bootstrap: :environment do
    Rake::Task["create_example:create_lessons"].invoke
    Rake::Task["create_example:create_and_associate_users"].invoke
  end

  task create_lessons: :environment do
    ActiveRecord::Base.transaction do
      Lesson.destroy_all
      100.times do |i|
        puts "Creating lesson #{i}"
        parent = Lesson.create(position: i, title: "Lesson #{i + 1}", content: "Here be content")
        3.times do |j|
          Lesson.create(position: j, title: "Part #{j + 1}", content: "Activities", parent: parent)
        end
      end
    end
  end

  task create_and_associate_users: :environment do
    ActiveRecord::Base.transaction do
      Cohort.destroy_all
      User.destroy_all
      StudentLesson.destroy_all
      year6 = Cohort.create(name: "Robertson State School - Year 6")
      year7 = Cohort.create(name: "Robertson State School - Year 7")
      lessons = Lesson.roots_and_descendants_preordered.pluck(:id)

      30.times.map do |i|
        puts "Creating student #{i}"
        student = Student.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email)
        [year6, year7].sample.users << student
        Random.rand(lessons.length - 1).times do |j|
          StudentLesson.create(student: student, lesson_id: lessons[j])
        end
      end

      4.times do |i|
        puts "Creating teacher #{i}"
        teacher = Teacher.create(first_name: Faker::Name.first_name, last_name: Faker::Name.last_name, email: Faker::Internet.email)
        [year6, year7].sample.users << teacher
      end
    end
  end
end
