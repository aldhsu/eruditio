FactoryGirl.define do
  factory :student do
    first_name "Student"
    last_name "User"
    email "email@example.com"
  end
end
