FactoryGirl.define do
  factory :teacher do
    first_name "Teacher"
    last_name "User"
    email "teacher@example.com"
  end
end
