require "rails_helper"

describe StudentsController do
  before do
    request.headers["Content-Type"] = "application/vnd.api+json"
  end

  let(:student) { FactoryGirl.create(:student) }

  describe "#show" do
    it "gets the user" do
      get :show, id: student.id
      expect(response.body).to include student.first_name
    end

    let(:lesson) { FactoryGirl.create(:lesson) }
    let!(:complete_lesson) { FactoryGirl.create(:student_lesson, student: student, lesson: lesson) }

    it "get the student with progress" do
      get :show, id: student.id, include: "student-lessons,lessons"
      json = JSON.parse(response.body)
      expect(json["included"]).to match [
        a_hash_including("id" => lesson.id.to_s, "type" => "lessons"),
        a_hash_including("id" => complete_lesson.id.to_s,  "type" => "student-lessons")
      ]
    end
  end
end
