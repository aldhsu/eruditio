require "rails_helper"

describe ReportsController do
  describe "#cohort" do
    context "has teacher" do
      let!(:teacher) { FactoryGirl.create(:teacher) }

      it "renders the report" do
        get :cohort, teacher_id: teacher.id

        expect(response.body).to include ""
        expect(response.status).to eq 200
      end
    end

    context "no teacher" do
      it "renders the report" do
        get :cohort, teacher_id: -1

        expect(response.status).to eq 404
      end
    end
  end
end
