# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  type       :string           not null
#  first_name :string           not null
#  last_name  :string           not null
#  email      :string           not null
#  created_at :datetime
#  updated_at :datetime
#

require "rails_helper"

describe Student do
  it { is_expected.to have_many(:lessons).through(:student_lessons) }
end
