require "rails_helper"

describe Teacher do
  describe "#class_reports" do
    let(:teacher) { FactoryGirl.create(:teacher) }
    let(:cohort)  { FactoryGirl.create(:cohort) }
    let(:student)  { FactoryGirl.create(:student) }
    let(:student2) { FactoryGirl.create(:student, first_name: "Student2") }
    let!(:student3) { FactoryGirl.create(:student, first_name: "Student3") }
    let(:lesson) { FactoryGirl.create(:lesson) }

    before do
      teacher.cohorts << cohort
      student.cohorts << cohort
      student2.cohorts << cohort
      student.lessons << lesson
    end

    it "reports on the number of completed lessons" do
      expect(teacher.class_reports).to match_array [
        { "count" => "1", "first_name" => "Student", "last_name" => "User" },
        { "count" => "0", "first_name" => "Student2", "last_name" => "User" }
      ]
    end

    it "does not contain students not in the teachers cohorts" do
      expect(Student.find_by(first_name: "Student3")).to be_truthy
      expect(teacher.class_reports.detect { |record| record["first_name"] == "Student3" }).to be_falsey
    end
  end
end
