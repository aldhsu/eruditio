# == Schema Information
#
# Table name: lessons
#
#  id         :integer          not null, primary key
#  parent_id  :integer
#  position   :integer          default(1), not null
#  content    :text
#  title      :text
#  created_at :datetime
#  updated_at :datetime
#

require "rails_helper"

describe Lesson do
  it { is_expected.to belong_to(:parent) }
  it { is_expected.to have_many(:parts).dependent(:destroy) }
  it { is_expected.to have_many(:students).through(:student_lessons) }
  it { is_expected.to validate_presence_of(:position) }
  it { is_expected.to validate_uniqueness_of(:position).scoped_to(:parent_id) }

  context "finding the next lesson" do
    let(:lesson) { FactoryGirl.create(:lesson) }
    let!(:part1) { FactoryGirl.create(:lesson, parent: lesson) }
    let!(:part2) { FactoryGirl.create(:lesson, parent: lesson, position: 2) }
    let!(:part3) { FactoryGirl.create(:lesson, parent: lesson, position: 3) }
    let!(:lesson2) { FactoryGirl.create(:lesson, position: 2) }
    let!(:second_lesson_part1) { FactoryGirl.create(:lesson, parent: lesson2) }
    let!(:second_lesson_part2) { FactoryGirl.create(:lesson, parent: lesson2, position: 2) }
    let!(:second_lesson_part3) { FactoryGirl.create(:lesson, parent: lesson2, position: 3) }

    context "preorder traversal" do
      describe "it acts as tree" do
        it "returns all the lessons in a pre order traversal" do
          expect(Lesson.flatten_in_preorder).to match [
            lesson.id,
            part1.id,
            part2.id,
            part3.id,
            lesson2.id,
            second_lesson_part1.id,
            second_lesson_part2.id,
            second_lesson_part3.id
          ]
        end

        context "arbitrary shaped lessons" do
          let!(:part2_sub_part) { Lesson.create(parent: part2) }

          it "returns the leaf nodes in order" do
            expect(Lesson.flatten_in_preorder).to match_array [
              lesson.id,
              part1.id,
              part2.id,
              part2_sub_part.id,
              part3.id,
              lesson2.id,
              second_lesson_part1.id,
              second_lesson_part2.id,
              second_lesson_part3.id
            ]
          end
        end
      end

      describe "#next_lesson_id" do
        it "retrieves the next lesson when it is part1" do
          expect(part1.next_lesson_id).to eq part2.id
        end

        it "retrieves the next lesson when it is part2" do
          expect(part2.next_lesson_id).to eq part3.id
        end

        it "retrieves the next lesson when it is going to upwards to a node" do
          expect(part3.next_lesson_id).to eq lesson2.id
        end
      end

      describe "#previous_lesson_id" do
        it "retrieves the nil when it is part1" do
          expect(lesson.previous_lesson_id).to be_nil
        end

        it "retrieves the previous lesson when it is part2" do
          expect(part2.previous_lesson_id).to eq part1.id
        end

        it "retrieves the next lesson when it is going to downwards to a node" do
          expect(lesson2.previous_lesson_id).to eq part3.id
        end
      end
    end

    context "leaf only traversal" do
      describe "#flatten_in_preorder" do
        it "returns the leaf nodes in order" do
          expect(Lesson.flatten_in_preorder(leaf_only: true)).to match_array [
            part1.id,
            part2.id,
            part3.id,
            second_lesson_part1.id,
            second_lesson_part2.id,
            second_lesson_part3.id
          ]
        end

        context "arbitrary shaped lessons" do
          let!(:part2_sub_part) { Lesson.create(parent: part2) }

          it "returns the leaf nodes in order" do
            expect(Lesson.flatten_in_preorder(leaf_only: true)).to match_array [
              part1.id,
              part2_sub_part.id,
              part3.id,
              second_lesson_part1.id,
              second_lesson_part2.id,
              second_lesson_part3.id
            ]
          end
        end
      end

      describe "leaf_node_index" do
        it "retrieves the next lesson when it is part1" do
          expect(part1.leaf_node_index(1)).to eq part2.id
        end

        it "retrieves the next lesson when it is part3" do
          expect(part3.leaf_node_index(1)).to eq second_lesson_part1.id
        end
      end
    end
  end
end
