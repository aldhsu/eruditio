# == Schema Information
#
# Table name: student_lessons
#
#  id         :integer          not null, primary key
#  student_id :integer          not null
#  lesson_id  :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

require "rails_helper"

describe StudentLesson do
  it { is_expected.to belong_to(:student) }
  it { is_expected.to belong_to(:lesson) }
  it { is_expected.to validate_presence_of(:student) }
  it { is_expected.to validate_presence_of(:lesson) }

  describe "#lesson_is_next" do
    let!(:student) { FactoryGirl.create(:student) }
    let!(:lesson) { FactoryGirl.create(:lesson) }
    let!(:part1) { FactoryGirl.create(:lesson, parent: lesson) }
    let!(:part2) { FactoryGirl.create(:lesson, parent: lesson, position: 2) }
    let!(:part3) { FactoryGirl.create(:lesson, parent: lesson, position: 3) }

    context "is the very first lesson" do
      it "is creatable" do
        new_completed_lesson = FactoryGirl.build(:student_lesson, lesson: lesson, student: student)
        new_completed_lesson.valid?
        expect(new_completed_lesson.valid?).to be_truthy
      end
    end

    context "a record of the previous lesson exists" do
      before do
        FactoryGirl.create(:student_lesson, lesson: lesson, student: student)
        FactoryGirl.create(:student_lesson, lesson: part1, student: student)
      end

      it "is creatable" do
        new_completed_lesson = FactoryGirl.build(:student_lesson, lesson: part2, student: student)
        new_completed_lesson.valid?
        expect(new_completed_lesson.valid?).to be_truthy
      end
    end

    context "is not the next lesson" do
      it "is not creatable" do
        new_completed_lesson = FactoryGirl.build(:student_lesson, lesson: part2, student: student)
        new_completed_lesson.valid?
        expect(new_completed_lesson.valid?).to be_falsey
        expect(new_completed_lesson.errors.messages.first).to eq [:base, ["You have not completed the previous lessons yet."]]
      end
    end
  end

  describe "#check_duplicate" do
    let!(:student) { FactoryGirl.create(:student) }
    let!(:lesson) { FactoryGirl.create(:lesson) }

    context "has duplicate" do
      before do
        FactoryGirl.create(:student_lesson, lesson: lesson, student: student)
      end

      it "is not a valid record" do
        new_completed_lesson = FactoryGirl.build(:student_lesson, lesson: lesson, student: student)
        new_completed_lesson.valid?
        expect(new_completed_lesson.valid?).to be_falsey
        expect(new_completed_lesson.errors.messages.first).to eq [:base, ["Duplicate record exists for this student and lesson."]]
      end
    end
  end
end
