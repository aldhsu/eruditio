# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  type       :string           not null
#  first_name :string           not null
#  last_name  :string           not null
#  email      :string           not null
#  created_at :datetime
#  updated_at :datetime
#

require "rails_helper"

describe User do
  it { is_expected.to validate_presence_of(:type) }
  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_presence_of(:email) }
  it { is_expected.to have_and_belong_to_many(:cohorts) }
end
